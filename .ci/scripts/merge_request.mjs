#!/usr/bin/env zx

import 'zx/globals';

$.verbose = false;

(async () => {
    const name = require('../../package.json').name;
    console.log(chalk.blue.bold(`started merging module: ${chalk.green.bold(name)}`));
    const module = name.replace('nano-core-', '');

    await $`git stash save --keep-index --include-untracked`;
    await $`git checkout release`;
    await $`git reset --hard origin/release`;

    const describe = await $`git describe --tags \`git rev-list --tags --max-count=1\``;

    console.log(chalk.blue.bold(`latest version: ${chalk.green.bold(describe.stdout)}`));

    const version = describe
        .stdout
        .replace('v', '')
        .replace(/(\r\n|\n|\r)/gm, '')
        .split('.')
        .map(e => parseInt(e));

    version[2]++;

    const newVersion = version
        .join('.');

    const branch = `version/${newVersion}`;

    console.log(chalk.blue.bold(`new tag: ${chalk.green.bold(newVersion)}`));
    console.log(chalk.blue.bold(`version branch: ${chalk.green.bold(branch)}`));

    await $`git checkout master`;

    console.log(chalk.blue.bold(`generating license...`));

    await $`rm package.json`;
    await $`yo-gen-run --name license --config .yo.json`;

    console.log(chalk.blue.bold(`moving files...`));

    cd('..');
    await $`mv ${module} /tmp/`;
    await $`mkdir ${module}`;
    await $`mv /tmp/${module}/.git ${module}/`;
    cd(module);

    await $`git checkout release`;
    await $`git fetch`;


    console.log(chalk.blue.bold(`checking if version branch exists...`));

    try {
        await $`git rev-parse --quiet --verify ${branch}`;

        console.log(chalk.blue.bold(`branch ${chalk.green.bold(branch)} already exists`));
        await $`git checkout ${branch}`;

    } catch {
        console.log(chalk.blue.bold(`creating branch: ${chalk.green.bold(branch)}`));
        await $`git checkout -b ${branch}`;

    }

    await $`git rm -rf .`;

    cd('..');

    const move = [
        `/tmp/${module}/.ci`,
        `/tmp/${module}/src`,
        `/tmp/${module}/.gitlab-ci.yml`,
        `/tmp/${module}/composer.json`,
        `/tmp/${module}/renovate.json`,
        `/tmp/${module}/README.md`,
        `/tmp/${module}/LICENSE`,
    ];

    for (const e of move) {
        await $`mv ${e} ${module}/`;
    }

    console.log(chalk.blue.bold(`adding changes to git...`));
    cd(module);

    try {
        await $`git add -A`;
        console.log(chalk.blue.bold(`processing commit: ${chalk.green.bold(process.env.CI_COMMIT_MESSAGE)}`));
        await $`git commit -m ${process.env.CI_COMMIT_MESSAGE}`;

        console.log(chalk.blue.bold(`pushing changes...`));
        await $`git push -f --set-upstream origin ${branch} -o merge_request.create -o merge_request.target=release -o merge_request.title="${branch}" -o merge_request.label="release ${newVersion}" -o merge_request.remove_source_branch`;

        console.log(chalk.blue.bold(`merge request created`));
    } catch {
        console.log(chalk.blue.bold(`nothing to commit...`));
    }
})();
