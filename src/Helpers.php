<?php

namespace laylatichy\nano\core;

use JetBrains\PhpStorm\Pure;
use laylatichy\Nano;

final class Helpers {
    #[Pure]
    public static function checkRequired(Nano $nano, array $required): void {
        $data = $nano->request->post();

        if (!is_array($data)) {
            throw new NanoException(
                message: HttpCode::BAD_REQUEST->name,
                code: HttpCode::BAD_REQUEST->code(),
            );
        }

        foreach ($required as $e) {
            $v = explode(":", $e);
            if (!in_array(needle: $v[0], haystack: array_keys($data), strict: true)) {
                throw new NanoException(
                    message: "missing required field: {$v[0]}",
                    code: HttpCode::BAD_REQUEST->code(),
                );
            } else {
                $type = gettype($data[$v[0]]);
                if (isset($v[1]) && $type !== $v[1]) {
                    throw new NanoException(
                        message: "required filed: {$v[0]} should be of a type {$v[1]}, {$type} given",
                        code: HttpCode::BAD_REQUEST->code(),
                    );
                }
            }
        }
    }

    #[Pure]
    public static function checkTypes(array $data, array $items): void {
        foreach ($items as $arg => $v) {
            if (isset($data[$arg])) {
                $types = explode('|', str_replace('?', '', $v));

                $type = gettype($data[$arg]);

                if (!in_array($type, array_values($types), true)) {
                    throw new NanoException(
                        message: "invalid type for: {$arg}, expected: {$v}, {$type} given",
                        code: HttpCode::BAD_REQUEST->code(),
                    );
                }
            }
        }
    }
}
