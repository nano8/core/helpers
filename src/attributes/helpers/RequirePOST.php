<?php

namespace laylatichy\nano\core\attributes\helpers;

use Attribute;
use Exception;
use InvalidArgumentException;
use laylatichy\Nano;
use laylatichy\nano\core\Helpers;
use laylatichy\nano\core\MiddlewareModule;
use ReflectionClass;
use ReflectionMethod;

#[Attribute(Attribute::TARGET_METHOD)]
final class RequirePOST {
    public static function get(ReflectionMethod $reflection, Nano $nano): ?MiddlewareModule {
        $attributes = $reflection->getAttributes(name: self::class);

        if (isset($attributes[0])) {
            $attribute = $attributes[0];

            if (isset($attribute->getArguments()[0])) {
                try {
                    $middleware           = new MiddlewareModule();
                    $middleware->class    = new ReflectionClass(objectOrClass: Helpers::class);
                    $middleware->method   = $middleware->class->getMethod(name: 'checkRequired');
                    $middleware->callback = fn ($request, ...$args) => $middleware->method->invoke(
                        $middleware->class->newInstanceWithoutConstructor(),
                        $nano,
                        $attribute->getArguments(),
                    );

                    return $middleware;
                } catch (Exception $e) {
                    throw new InvalidArgumentException(
                        message: $e->getMessage()
                    );
                }
            } else {
                throw new InvalidArgumentException(
                    message: 'invalid argument for ' . __CLASS__ . ' attribute'
                );
            }
        }

        return null;
    }
}
