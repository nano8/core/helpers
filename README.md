### nano/core/helpers

[![release](https://img.shields.io/packagist/v/laylatichy/nano-core-helpers?style=flat-square)](https://packagist.org/packages/laylatichy/nano-core-helpers)
[![PHPStan](https://img.shields.io/badge/PHPStan-Level%20max-brightgreen.svg?style=flat&logo=php)](https://shields.io/#/)
[![Total Downloads](https://img.shields.io/packagist/dt/laylatichy/nano-core-helpers?style=flat-square)](https://packagist.org/packages/laylatichy/nano-core-helpers)

##### CI STATUS

-   dev [![pipeline status](https://gitlab.com/nano8/core/helpers/badges/dev/pipeline.svg)](https://gitlab.com/nano8/core/helpers/-/commits/dev)
-   master [![pipeline status](https://gitlab.com/nano8/core/helpers/badges/master/pipeline.svg)](https://gitlab.com/nano8/core/helpers/-/commits/master)
-   release [![pipeline status](https://gitlab.com/nano8/core/helpers/badges/release/pipeline.svg)](https://gitlab.com/nano8/core/helpers/-/commits/release)

#### Install

-   `composer require laylatichy/nano-core-helpers`
